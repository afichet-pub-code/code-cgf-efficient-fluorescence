#!/bin/sh

git clone --depth 1 git://cgg.mff.cuni.cz/ART.git
cp gmm.cgf.patch ART
cd ART
git apply gmm.cgf.patch
git remote add upstream https://gitlab.com/art-development-team/ART.git