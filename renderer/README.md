ART patch
=========

This folder contains a patch for ART to support the GMM representation
for fluorescent spectra. It is based on ART v2.0.3. It also contains a
bash script to apply the patch automatically and get the same source
tree as the one used in the paper.


Automatic patching
------------------

If you want to get a working copy of ART supporting the added
compressed GMM fluorescent spectra, execute the script
`make_art_gmm.sh`.


Manual patching
---------------

The patch is named `gmm.cgf.patch`. First checkout the current ART public
release:

```bash
git clone --depth 1 git://cgg.mff.cuni.cz/ART.git
```

Then, apply the patch:

```bash
cp gmm.cgf.patch ART
cd ART
git apply gmm.cgf.patch
```

Compiling ART
-------------

When the patch is applied, you can compile ART as usual, for example:

```bash
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=~/.local -DCMAKE_BUILD_TYPE=Release
make -j16 install
```

Now, you can use the GMM representation by loading the spectra with
`GMM_FIT("filename")`.

