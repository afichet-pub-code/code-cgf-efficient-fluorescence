set terminal epslatex size 5.5cm,4.5cm lw 2
set output output_file

set xlabel "wavelength [nm]"
set xtics 150

set xrange [300:780]

p data_absorption w l t 'abs', data_reemission w l t 'reem'

set output