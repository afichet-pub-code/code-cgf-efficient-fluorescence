set terminal epslatex size 7.5cm,5cm lw 2
set output output_file

set xrange [0:40]
set yrange [0:32]
set cbrange [0:0.05]

set size ratio -1

set xtics ('300' 0, '500' 20, '700' 40)
set ytics ('400' 2, '500' 12, '600' 22, '700' 32)

#set xlabel "$\\lambda_i$ [nm]"
#set ylabel "$\\lambda_o$ [nm]"
set cblabel offset 1.5, 0, 0

set palette defined ( 0 "#000090",\
                      1 "#000fff",\
                      2 "#0090ff",\
                      3 "#0fffee",\
                      4 "#90ff70",\
                      5 "#ffee00",\
                      6 "#ff7000",\
                      7 "#ee0000",\
                      8 "#7f0000")

plot data_file matrix w image noti

set output
