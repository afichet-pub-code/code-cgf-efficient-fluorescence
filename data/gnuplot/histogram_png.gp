set terminal pngcairo size 6000, 640 font 'Verdana, 8'
set output output_file

set style data histogram
set xtics rotate by -45 scale 0
set style fill solid border -1

plot data_file using 2:xtic(1)

set output