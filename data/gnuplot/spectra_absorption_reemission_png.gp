set terminal pngcairo
set output output_file

set xlabel "wavelength [nm]"

p data_absorption w l t 'absorption', data_reemission w l t 'reemission'

set output