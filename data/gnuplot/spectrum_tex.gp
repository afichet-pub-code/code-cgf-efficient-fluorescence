set terminal epslatex size 5.5cm,4.5cm lw 2
set output output_file

set xlabel "wavelength [nm]"
set xtics 150

set xrange [380:780]

p data_file w l noti

set output