set terminal epslatex size 5.5cm,4.5cm lw 2
set output output_file

set xrange [0:48]
set yrange [0:40]
set cbrange [0:0.06]

set xtics ("300" 0, "540" 24, "780" 48)
set ytics ("380" 0, "580" 20, "780" 40)

# unset xtics
# unset ytics
# unset cbtics
# unset colorbox

# set lmargin 0
# set rmargin 0
# set tmargin 0
# set bmargin 0


set palette defined ( 0 "#000000",\
                      1 "#000fff",\
                      2 "#0090ff",\
                      3 "#0fffee",\
                      4 "#90ff70",\
                      5 "#ffee00",\
                      6 "#ff7000",\
                      7 "#ee0000",\
                      8 "#7f0000")


p data_file matrix w image noti

set output
