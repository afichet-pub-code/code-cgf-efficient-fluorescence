import os, subprocess

class Gnuplot:

    def __init__(self):
        current_dir = os.path.dirname(os.path.realpath(__file__))
        self.asset_dir = os.path.join(current_dir, '..','..','data', 'gnuplot')


    def plot_reradiation_png(self, input_matrix, output_png):
        subprocess.call(
            [
                'gnuplot',
                '-e',
                "output_file='{}';data_file='{}'".format(
                    output_png, input_matrix
                ),
                os.path.join(self.asset_dir,'rerad_png.gp')
            ]
        )


    def plot_reradiation_tex(self, input_matrix, output_tex):
        out_file = os.path.basename(output_tex)
        cwd      = os.path.dirname(output_tex)

        subprocess.call(
            [
                'gnuplot',
                '-e',
                "output_file='{}';data_file='{}'".format(
                    out_file, input_matrix
                ),
                os.path.join(self.asset_dir,'rerad_tex.gp')
            ],
            cwd=cwd
        )


    def plot_spectrum_png(self, input_spectrum, output_png):
        subprocess.call(
            [
                'gnuplot',
                '-e',
                "output_file='{}';data_file='{}'".format(
                    output_png, input_spectrum
                ),
                os.path.join(self.asset_dir,'spectrum_png.gp')
            ]
        )


    def plot_spectrum_tex(self, input_spectrum, output_tex):
        out_file = os.path.basename(output_tex)
        cwd      = os.path.dirname(output_tex)

        subprocess.call(
            [
                'gnuplot',
                '-e',
                "output_file='{}';data_file='{}'".format(
                    out_file, input_spectrum
                ),
                os.path.join(self.asset_dir,'spectrum_tex.gp')
            ],
            cwd=cwd
        )

    
    def plot_absorption_reemission_png(self, absorption_spectrum, reemission_spectrum, output_png):
        subprocess.call(
            [
                'gnuplot',
                '-e',
                "output_file='{}';data_absorption='{}';data_reemission='{}'".format(
                    output_png, absorption_spectrum, reemission_spectrum
                ),
                os.path.join(self.asset_dir,'spectra_absorption_reemission_png.gp')
            ]
        )

    
    def plot_absorption_reemission_tex(self, absorption_spectrum, reemission_spectrum, output_tex):
        out_file = os.path.basename(output_tex)
        cwd      = os.path.dirname(output_tex)

        subprocess.call(
            [
                'gnuplot',
                '-e',
                "output_file='{}';data_absorption='{}';data_reemission='{}'".format(
                    out_file, absorption_spectrum, reemission_spectrum
                ),
                os.path.join(self.asset_dir,'spectra_absorption_reemission_tex.gp')
            ],
            cwd=cwd
        )


    def plot_histogram_summary_png(self, input_data, output_png):
        subprocess.call(
            [
                'gnuplot',
                '-e',
                "output_file='{}';data_file='{}'".format(
                    output_png, input_data
                ),
                os.path.join(self.asset_dir,'histogram_png.gp')
            ]
        )


    def plot_histogram_summary_tex(self, input_data, output_tex):
        out_file = os.path.basename(output_tex)
        cwd      = os.path.dirname(output_tex)

        subprocess.call(
            [
                'gnuplot',
                '-e',
                "output_file='{}';data_file='{}'".format(
                    out_file, input_data
                ),
                os.path.join(self.asset_dir,'histogram_tex.gp')
            ],
            cwd=cwd
        )