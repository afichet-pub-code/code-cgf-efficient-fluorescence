#!/usr/bin/env python3

import operator
import numpy as np
import matplotlib.pyplot as plt

from radiometry.fluospectrum import FluoSpectrum
from radiometry.spectrum import Spectrum, TabSpectrum
from radiometry.cmf import CMF, XYZ_to_sRGB, to_sRGB_gamma
from radiometry.colortools import deltaE2000_XYZ

import tools.turbo_colormap as turbo

from PIL import Image

def plot_rerad(plot, title, fluo_spectrum, reradiation):
    fluo_bounds = (
        fluo_spectrum.wavelength_i_start,
        fluo_spectrum.wavelength_i_end,
        fluo_spectrum.wavelength_o_end,
        fluo_spectrum.wavelength_o_start,
    )
    plot.imshow(reradiation, extent=fluo_bounds)
    plot.set_title(title)
    plot.set_xlabel("$\\lambda_i$")
    plot.set_ylabel("$\\lambda_o$")
    plot.invert_yaxis()

    
def plot_absorption_reemission_average(fluo_spectrum: FluoSpectrum):
    avg_abs = fluo_spectrum.get_average_absorption_spectrum()
    avg_ree = fluo_spectrum.get_average_reemission_spectrum()

    # Get the index where the maximum average absorption and reemission are reached
    idx_max_abs, _ = max(enumerate(avg_abs), key=operator.itemgetter(1))
    idx_max_ree, _ = max(enumerate(avg_ree), key=operator.itemgetter(1))

    # Retrieved the wavelength of those max
    max_abs = fluo_spectrum.wl_for_idx_in(idx_max_abs)
    max_ree = fluo_spectrum.wl_for_idx_out(idx_max_ree)

    plt.plot(
        fluo_spectrum.get_excitation_wavelengths(),
        avg_abs,
        label=f"Absorption (max: {max_abs} nm)"
    )
    plt.plot(
        fluo_spectrum.get_reflectance_wavelengths(),
        avg_ree,
        label=f"Reemission (max: {max_ree} nm)"
    )
    
    plt.xlabel("$\\lambda\:[\mathrm{nm}]$")
    plt.legend()

    
def plot_absorption_reemission(fluo_spectrum: FluoSpectrum):
    pure_fluo = fluo_spectrum.get_pure_fluo_filtered()

    # Compute the sum of lines and columns
    sum_abs = np.sum(pure_fluo, axis=0)
    sum_refl = np.sum(pure_fluo, axis=1)

    # Get the index where the maximum absorption and reemission are reached
    idx_max_abs, _ = max(enumerate(sum_abs), key=operator.itemgetter(1))
    idx_max_ree, _ = max(enumerate(sum_refl), key=operator.itemgetter(1))

    # Retrieved the wavelength of those max
    max_abs = fluo_spectrum.wl_for_idx_in(idx_max_abs)
    max_ree = fluo_spectrum.wl_for_idx_out(idx_max_ree)

    plt.plot(
        fluo_spectrum.get_excitation_wavelengths(),
        sum_abs,
        label=f"Absorption (max: {max_abs} nm)"
    )
    plt.plot(
        fluo_spectrum.get_reflectance_wavelengths(),
        sum_refl,
        label=f"Reemission (max: {max_ree} nm)"
    )
    
    plt.xlabel("$\\lambda\:[\mathrm{nm}]$")
    plt.legend()


def plot_photon_energy(fluo_spectrum: FluoSpectrum):
    # Constants
    h = 6.62607015e-34
    c = 2.99792458e8

    pure_fluo = fluo_spectrum.get_pure_fluo_filtered()

    # Compute the sum of lines and columns
    sum_abs = np.sum(pure_fluo, axis=0)
    sum_refl = np.sum(pure_fluo, axis=1)

    refl_wavelengths = fluo_spectrum.get_reflectance_wavelengths()
    refl_energy = [h * c / wavelength for wavelength in refl_wavelengths]

    fig, ax_wl = plt.subplots()
    ax_en = ax_wl.twiny()

    color_ax_wl = "tab:red"
    color_ax_en = "tab:blue"

    ax_wl.set_xlabel("$\\lambda\:[\\mathrm{nm}]$", color=color_ax_wl)
    ax_en.set_xlabel("$E\:[\mathrm{J}]$", color=color_ax_en)

    ax_wl.tick_params(axis="x", labelcolor=color_ax_wl)
    ax_en.tick_params(axis="x", labelcolor=color_ax_en)

    ax_wl.plot(refl_wavelengths, sum_refl, color=color_ax_wl)
    ax_en.plot(refl_energy, sum_refl, color=color_ax_en)

    
def create_image_patch(
    fluo_spectrum: FluoSpectrum, 
    illuminant_spectrum: Spectrum, cmf: CMF, 
    m, width = 100, height = 100) -> Image:  
    RGB = XYZ_to_sRGB(cmf.get_xyz_reflective_fluo(fluo_spectrum, illuminant_spectrum))
    R, G, B = np.clip(RGB / m, 0, 1)
    R, G, B = to_sRGB_gamma(R), to_sRGB_gamma(G), to_sRGB_gamma(B)
    
    px = np.ones((height, width, 3))
    px[:, :, 0] *= R * 255
    px[:, :, 1] *= G * 255
    px[:, :, 2] *= B * 255
    
    return Image.fromarray(px.astype(np.uint8))
    

def create_image_patch_cmp(
    fluo_spectrum_1: FluoSpectrum, fluo_spectrum_2: FluoSpectrum, 
    illuminant_spectrum: Spectrum, cmf: CMF, 
    m, width = 100, height = 100) -> Image:

    """ Compares two reflectance by setting the second spectrum in a the middle of the first """
#     RGB_1 = XYZ_to_sRGB(cmf.get_xyz_reflective_fluo(fluo_spectrum_1, illuminant_spectrum))
#     R_1, G_1, B_1 = np.clip(RGB_1 / m, 0, 1)
#     R_1, G_1, B_1 = to_sRGB_gamma(R_1), to_sRGB_gamma(G_1), to_sRGB_gamma(B_1)
    
#     RGB_2 = XYZ_to_sRGB(cmf.get_xyz_reflective_fluo(fluo_spectrum_2, illuminant_spectrum))
#     R_2, G_2, B_2 = np.clip(RGB_2 / m, 0, 1)
#     R_2, G_2, B_2 = to_sRGB_gamma(R_2), to_sRGB_gamma(G_2), to_sRGB_gamma(B_2)
    
    
    XYZ_1 = cmf.get_xyz_reflective_fluo(fluo_spectrum_1, illuminant_spectrum)
    RGB_1 = XYZ_to_sRGB(XYZ_1 / m)
    R_1, G_1, B_1 = np.clip(RGB_1, 0, 1)
    R_1, G_1, B_1 = to_sRGB_gamma(R_1), to_sRGB_gamma(G_1), to_sRGB_gamma(B_1)
    
    XYZ_2 = cmf.get_xyz_reflective_fluo(fluo_spectrum_2, illuminant_spectrum)
    RGB_2 = XYZ_to_sRGB(XYZ_2 / m)
    R_2, G_2, B_2 = np.clip(RGB_2, 0, 1)
    R_2, G_2, B_2 = to_sRGB_gamma(R_2), to_sRGB_gamma(G_2), to_sRGB_gamma(B_2)
    
    
    
    px = np.ones((height, width, 3))
    px[:, :, 0] *= R_1 * 255
    px[:, :, 1] *= G_1 * 255
    px[:, :, 2] *= B_1 * 255
    
    px[height//4:3*height//4, width//4:3*width//4, 0] = R_2 * 255
    px[height//4:3*height//4, width//4:3*width//4, 1] = G_2 * 255
    px[height//4:3*height//4, width//4:3*width//4, 2] = B_2 * 255
    
    return Image.fromarray(px.astype(np.uint8))


def create_image_colorchart(
    reflectance, illuminant_spectrum: Spectrum, 
    cmf: CMF, m, 
    rows = 4, cols = 6, 
    sep = 2, margin = 5, 
    width_patch = 50, height_patch = 50) -> Image:

    sRGB_patches = []
    
    for c in reflectance[:rows * cols]:
        RGB = XYZ_to_sRGB(cmf.get_xyz_reflective(c, illuminant_spectrum))

        # Clamp in range [0..1]
        R, G, B = np.clip(RGB / m, 0, 1)

        # gamma correction
        R, G, B = to_sRGB_gamma(R), to_sRGB_gamma(G), to_sRGB_gamma(B)

        sRGB_patches.append([R, G, B])
    
    width = 2 * margin + cols * width_patch + (cols - 1) * sep
    height = 2 * margin + rows * height_patch + (rows - 1) * sep

    px = np.zeros((height, width, 3))
    
    start_y = margin
    
    for y in range(rows):
        end_y   = start_y + height_patch

        start_x = margin

        for x in range(cols):
            end_x   = start_x + width_patch
                
            idx_patch = int(cols * y + x)
            
            if idx_patch < len(sRGB_patches):
                R, G, B = sRGB_patches[idx_patch]

                px[start_y:end_y, start_x:end_x, 0] = R * 255
                px[start_y:end_y, start_x:end_x, 1] = G * 255
                px[start_y:end_y, start_x:end_x, 2] = B * 255

            start_x += width_patch + sep
            
        start_y += height_patch + sep
                
    return Image.fromarray(px.astype(np.uint8))


def create_image_colorchart_fluo(
    fluo_refl, illuminant_spectrum: Spectrum, 
    cmf: CMF, m, 
    rows = 4, cols = 6, 
    sep = 2, margin = 5, 
    width_patch = 50, height_patch = 50) -> Image:

    sRGB_patches = []
    
    for c in fluo_refl[:rows * cols]:
        RGB = XYZ_to_sRGB(cmf.get_xyz_reflective_fluo(c, illuminant_spectrum))

        # Clamp in range [0..1]
        R, G, B = np.clip(RGB / m, 0, 1)

        # gamma correction
        R, G, B = to_sRGB_gamma(R), to_sRGB_gamma(G), to_sRGB_gamma(B)

        sRGB_patches.append([R, G, B])
    
    width = 2 * margin + cols * width_patch + (cols - 1) * sep
    height = 2 * margin + rows * height_patch + (rows - 1) * sep

    px = np.zeros((height, width, 3))
    
    start_y = margin
    
    for y in range(rows):
        end_y   = start_y + height_patch

        start_x = margin

        for x in range(cols):
            end_x   = start_x + width_patch
                
            idx_patch = int(cols * y + x)
            
            if idx_patch < len(sRGB_patches):
                R, G, B = sRGB_patches[idx_patch]

                px[start_y:end_y, start_x:end_x, 0] = R * 255
                px[start_y:end_y, start_x:end_x, 1] = G * 255
                px[start_y:end_y, start_x:end_x, 2] = B * 255

            start_x += width_patch + sep
            
        start_y += height_patch + sep
                
    return Image.fromarray(px.astype(np.uint8))


def create_image_colorchart_fluo_cmp(
    fluo_refl_0, fluo_refl_1, 
    illuminant_spectrum: Spectrum, cmf: CMF, m, 
    rows = 4, cols = 6, 
    sep = 2, margin = 5, 
    width_patch = 50, height_patch = 50) -> Image:

    sRGB_patches_0 = []
    sRGB_patches_1 = []
    
    if len(fluo_refl_0) != len(fluo_refl_1):
        raise RuntimeError('Dimensions missmatch')
    
    for c in fluo_refl_0[:rows * cols]:
        RGB = XYZ_to_sRGB(cmf.get_xyz_reflective_fluo(c, illuminant_spectrum))

        # Clamp in range [0..1]
        R, G, B = np.clip(RGB / m, 0, 1)

        # gamma correction
        R, G, B = to_sRGB_gamma(R), to_sRGB_gamma(G), to_sRGB_gamma(B)

        sRGB_patches_0.append([R, G, B])
    
    
    for c in fluo_refl_1[:rows * cols]:
        RGB = XYZ_to_sRGB(cmf.get_xyz_reflective_fluo(c, illuminant_spectrum))

        # Clamp in range [0..1]
        R, G, B = np.clip(RGB / m, 0, 1)

        # gamma correction
        R, G, B = to_sRGB_gamma(R), to_sRGB_gamma(G), to_sRGB_gamma(B)

        sRGB_patches_1.append([R, G, B])
    
    width = 2 * margin + cols * width_patch + (cols - 1) * sep
    height = 2 * margin + rows * height_patch + (rows - 1) * sep

    px = np.zeros((height, width, 3))
    
    start_y = margin
    
    for y in range(rows):
        end_y   = start_y + height_patch

        start_y_cmp = start_y + height_patch // 4
        end_y_cmp   = end_y - height_patch // 4

        start_x = margin

        for x in range(cols):
            end_x   = start_x + width_patch
            
            start_x_cmp = start_x + width_patch // 4
            end_x_cmp   = end_x - width_patch // 4
            
            idx_patch = int(cols * y + x)
            
            if idx_patch < len(sRGB_patches_0):
                R, G, B = sRGB_patches_0[idx_patch]

                px[start_y:end_y, start_x:end_x, 0] = R * 255
                px[start_y:end_y, start_x:end_x, 1] = G * 255
                px[start_y:end_y, start_x:end_x, 2] = B * 255
                
                R, G, B = sRGB_patches_1[idx_patch]

                px[start_y_cmp:end_y_cmp, start_x_cmp:end_x_cmp, 0] = R * 255
                px[start_y_cmp:end_y_cmp, start_x_cmp:end_x_cmp, 1] = G * 255
                px[start_y_cmp:end_y_cmp, start_x_cmp:end_x_cmp, 2] = B * 255

            start_x += width_patch + sep
            
        start_y += height_patch + sep
                
    return Image.fromarray(px.astype(np.uint8))


# Utility for functions bellow

def _get_XYZ_from_fluo_for_mono(wl, ex_wl, refl_wl, pure_fluo, fluo_spectrum: FluoSpectrum, cmf: CMF):
    idx_wl_i_low  = fluo_spectrum.idx_for_wl_in(wl)
    idx_wl_i_high = idx_wl_i_low + 1

    wl_i_low  = fluo_spectrum.wl_for_idx_in(idx_wl_i_low)
    wl_i_high = fluo_spectrum.wl_for_idx_in(idx_wl_i_high)

    interp = (wl - wl_i_low) / (wl_i_high - wl_i_low)

    # trapezoidal interpolation delta
    d_wl_i = wl_i_high - wl_i_low

    # Needed for diagonal
    idx_wl_o_low  = fluo_spectrum.idx_for_wl_out(wl)
    idx_wl_o_high = idx_wl_o_low + 1

    if wl >= ex_wl[0] and wl <= ex_wl[-1]:
        interp_fluo = np.zeros(refl_wl.shape)
        interp_diag = 0

        if idx_wl_i_low >= 0 and idx_wl_i_low < pure_fluo.shape[1]:
            interp_fluo  += (1 - interp) * pure_fluo[:, idx_wl_i_low]

            if idx_wl_o_low >= 0 and idx_wl_o_low < fluo_spectrum.data.shape[0]:
                interp_diag += (1 - interp) * fluo_spectrum.data[idx_wl_o_low, idx_wl_i_low]

        if idx_wl_i_high < pure_fluo.shape[1] and idx_wl_i_high >= 0:
            interp_fluo  += interp * pure_fluo[:, idx_wl_i_high]

            if idx_wl_o_high >= 0 and idx_wl_o_high < fluo_spectrum.data.shape[0]:
                interp_diag += interp * fluo_spectrum.data[idx_wl_o_high, idx_wl_i_high]

        refl_fluo_spectrum_mono = TabSpectrum(
            refl_wl,
            interp_fluo
        )

        XYZ_fluo  = np.array(cmf.get_xyz_emissive(refl_fluo_spectrum_mono)) / d_wl_i
        XYZ_diag = np.array(cmf.get_xyz_emissive(TabSpectrum([wl], [interp_diag])))

        return XYZ_fluo + XYZ_diag
    else:
        return np.zeros((3, ))

    
def _get_RGB_from_fluo_for_mono(wl, ex_wl, refl_wl, pure_fluo, fluo_spectrum: FluoSpectrum, cmf: CMF):
    XYZ = _get_XYZ_from_fluo_for_mono(wl, ex_wl, refl_wl, pure_fluo, fluo_spectrum, cmf)
    
    RGB = XYZ_to_sRGB(XYZ)
    RGB = np.array([to_sRGB_gamma(c) for c in RGB])
    
    return RGB * 255
    
    
def create_colorbar_fluo(
    fluo_spectrum: FluoSpectrum,
    cmf: CMF, width = 100, height = 50, start_wl = 400, end_wl = 800) -> Image:
    
    px = np.zeros((height, width, 3))
    
    refl_wl = fluo_spectrum.get_reflectance_wavelengths()
    ex_wl  = fluo_spectrum.get_excitation_wavelengths()
    
    pure_fluo = fluo_spectrum.get_pure_fluo_filtered()
    
    for x in range(width):
        u = x / (width - 1)
        wl = (end_wl - start_wl) * u + start_wl
        
        RGB = _get_RGB_from_fluo_for_mono(wl, ex_wl, refl_wl, pure_fluo, fluo_spectrum, cmf)
        
        for c in range(3):
            px[:, x, c] = RGB[c]
            
    return Image.fromarray(px.astype(np.uint8))


def create_colorbar_fluo_cmp(
    fluo_spectrum_1: FluoSpectrum,
    fluo_spectrum_2: FluoSpectrum,
    cmf: CMF, 
    width = 100, height = 50, 
    start_wl = 400, end_wl = 800) -> Image:
    
    px = np.zeros((height, width, 3))

    refl_wl_1 = fluo_spectrum_1.get_reflectance_wavelengths()
    ex_wl_1  = fluo_spectrum_1.get_excitation_wavelengths()
    pure_fluo_1 = fluo_spectrum_1.get_pure_fluo_filtered()
    
    refl_wl_2 = fluo_spectrum_2.get_reflectance_wavelengths()
    ex_wl_2  = fluo_spectrum_2.get_excitation_wavelengths()
    pure_fluo_2 = fluo_spectrum_2.get_pure_fluo_filtered()
    
    for x in range(width):
        u = x / (width - 1)
        wl = round((end_wl - start_wl) * u + start_wl)
        
        RGB_1 = _get_RGB_from_fluo_for_mono(wl, ex_wl_1, refl_wl_1, pure_fluo_1, fluo_spectrum_1, cmf)
        RGB_2 = _get_RGB_from_fluo_for_mono(wl, ex_wl_2, refl_wl_2, pure_fluo_2, fluo_spectrum_2, cmf)
        
        for c in range(3):
            px[0:height//2, x, c] = RGB_1[c]
            px[height//2: , x, c] = RGB_2[c]
        
    return Image.fromarray(px.astype(np.uint8))


def mix(a, b, v):
    return (1 - v)*a + v*b


def place(v_min, v_max, v):
    v = min(v_max, max(v_min, v))
    return (v - v_min) / (v_max - v_min)


def scale(A, v):
    step = len(A)
    
    if v <= 1/step:
        i = place(0, 1/step, v)
        return (i*A[0][0], i*A[0][1], i*A[0][2])
    
    for n in range(1, step):
        if v <= (n+1) / (step):
            i = place(n/step, (n+1)/step, v)
            c = (mix(A[n-1][0], A[n][0], i), mix(A[n-1][1], A[n][1], i), mix(A[n-1][2], A[n][2], i))
            d =  np.sqrt(c[0]**2 + c[1]**2 + c[2]**2)
            
            c = [(x) for x in c]
                
            return c #(c[0]/d, c[1]/d, c[2]/d)
    return A[-1]


def get_rgb(v_min, v_max, v):
    v = place(v_min, v_max, v)

    # a0 = (0, 0, 1)
    # a1 = (0, 1, 1)
    # a2 = (0, 1, 0)
    # a3 = (1, 1, 0)
    # a4 = (1, 0, 0)

    # col = scale([a0, a1, a2, a3, a4], v)
    col = turbo.interpolate(turbo.turbo_colormap_data, v)
    
    return col


def create_h_scale(width, height) -> Image:
    px = np.zeros((height, width, 3))
    
    for x in range(width):
        v = get_rgb(0, 1, x / (width - 1))
        v = np.array(v) * 255
        
        for c in range(3):
            px[:, x, c] = v[c]
    
    return Image.fromarray(px.astype(np.uint8))


def create_v_scale(width, height) -> Image:
    px = np.zeros((height, width, 3))
    
    for y in range(height):
        v = get_rgb(0, 1, 1 - y / (height - 1))
        v = np.array(v) * 255
        
        for c in range(3):
            px[y, :, c] = v[c]
    
    return Image.fromarray(px.astype(np.uint8))


def create_colorbar_fluo_cmp_delta_e(
    fluo_spectrum_1: FluoSpectrum,
    fluo_spectrum_2: FluoSpectrum,
    cmf: CMF,
    width = 100, height = 50, 
    start_wl = 400, end_wl = 800,
    v_min = 0, v_max = 2, auto=False) -> Image:
    
    px = np.zeros((height, width, 3))
    
    refl_wl_1 = fluo_spectrum_1.get_reflectance_wavelengths()
    ex_wl_1  = fluo_spectrum_1.get_excitation_wavelengths()
    pure_fluo_1 = fluo_spectrum_1.get_pure_fluo_filtered()
    
    refl_wl_2 = fluo_spectrum_2.get_reflectance_wavelengths()
    ex_wl_2  = fluo_spectrum_2.get_excitation_wavelengths()
    pure_fluo_2 = fluo_spectrum_2.get_pure_fluo_filtered()
    
    dE_wl = []
    dE_v = []
    rgb_1 = []
    rgb_2 = []

    for x in range(width):
        u = x / (width - 1)
        wl = round((end_wl - start_wl) * u + start_wl)
        
        XYZ_1 = _get_XYZ_from_fluo_for_mono(wl, ex_wl_1, refl_wl_1, pure_fluo_1, fluo_spectrum_1, cmf)
        XYZ_2 = _get_XYZ_from_fluo_for_mono(wl, ex_wl_2, refl_wl_2, pure_fluo_2, fluo_spectrum_2, cmf)
        
        RGB_1 = XYZ_to_sRGB(XYZ_1)
        rgb_1.append(np.array([to_sRGB_gamma(c) for c in RGB_1]) * 255)

        RGB_2 = XYZ_to_sRGB(XYZ_2)
        rgb_2.append(np.array([to_sRGB_gamma(c) for c in RGB_2]) * 255)
        
        deltaE = deltaE2000_XYZ(XYZ_1, XYZ_2)
        
        dE_wl.append(wl)
        dE_v.append(deltaE)
        
    if auto:
        v_max = max(dE_v)

    for x in range(width):
        for c in range(3):
            deltaE_RGB = get_rgb(v_min, v_max, dE_v[x])
            deltaE_RGB = np.array(deltaE_RGB) * 255

            px[0              : height//3       , x, c] = rgb_1[x][c]
            px[height//3      : (2 * height)//3 , x, c] = rgb_2[x][c]
            px[(2 * height)//3:                 , x, c] = deltaE_RGB[c]
        
    return Image.fromarray(px.astype(np.uint8)), TabSpectrum(dE_wl, dE_v), v_max


def create_colorbar_fluo_cmp_normalized(
    fluo_spectrum_1: FluoSpectrum,
    fluo_spectrum_2: FluoSpectrum,
    cmf: CMF, width = 100, height = 50, start_wl = 400, end_wl = 800) -> Image:
    
    fluo_col_1 = np.zeros((width, 3))
    fluo_col_2 = np.zeros((width, 3))
    
    for x in range(width):
        u = x / (width - 1)
        wl = round((end_wl - start_wl) * u + start_wl)   
        fluo_col_1[x] = XYZ_to_sRGB(cmf.get_xyz_reflective_fluo(fluo_spectrum_1, TabSpectrum([wl - 10, wl, wl + 10], [0, 0.1, 0])))
        fluo_col_2[x] = XYZ_to_sRGB(cmf.get_xyz_reflective_fluo(fluo_spectrum_2, TabSpectrum([wl - 10, wl, wl + 10], [0, 0.1, 0])))
    
    min_total = min(np.min(fluo_col_1), np.min(fluo_col_2))
    if (min_total < 0):
        fluo_col_1 = fluo_col_1 + np.abs(min_total)
        fluo_col_2 = fluo_col_2 + np.abs(min_total)
    
    max_total = max(np.max(fluo_col_1), np.max(fluo_col_2))
    if (max_total > 1):
        fluo_col_1 = fluo_col_1 / max_total
        fluo_col_2 = fluo_col_2 / max_total

    fluo_col_1 = np.clip(fluo_col_1, 0, 1)
    fluo_col_2 = np.clip(fluo_col_2, 0, 1)

    px = np.zeros((height, width, 3))
    for x in range(width):
        minv = np.min(fluo_col_1[x])
        R, G, B = fluo_col_1[x] - minv
        R, G, B = to_sRGB_gamma(R), to_sRGB_gamma(G), to_sRGB_gamma(B)
        px[0:height//2, x, 0] = R * 255
        px[0:height//2, x, 1] = G * 255
        px[0:height//2, x, 2] = B * 255
        
        minv = np.min(fluo_col_2[x])
        R, G, B = fluo_col_2[x] - minv
        R, G, B = to_sRGB_gamma(R), to_sRGB_gamma(G), to_sRGB_gamma(B)
        px[height//2:, x, 0] = R * 255
        px[height//2:, x, 1] = G * 255
        px[height//2:, x, 2] = B * 255

    return Image.fromarray(px.astype(np.uint8))