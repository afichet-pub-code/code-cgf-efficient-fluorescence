import os, subprocess

class ART:
    env = os.environ.copy()
    seed = 0

    def __init__(self, binary_paths = [], library_path = '', resource_path = '', seed = 512):
        if len(binary_paths) > 0:
            self.env['PATH'] = (':'.join(binary_paths))[1:] + ':' + self.env['PATH']

        if library_path != '':
            self.env['LD_LIBRARY_PATH']  = library_path + ':' + self.env['LD_LIBRARY_PATH']
            self.env['ART_LIBRARY_PATH'] = library_path

        if resource_path != '':
            self.env['ART_RESOURCE_PATH'] = resource_path

        self.seed = seed


    def print_env(self):
        print('PATH              = {}'.format(self.env['PATH']))
        print('LD_LIBRARY_PATH   = {}'.format(self.env['LD_LIBRARY_PATH']))
        print('ART_LIBRARY_PATH  = {}'.format(self.env['ART_LIBRARY_PATH']))
        print('ART_RESOURCE_PATH = {}'.format(self.env['ART_RESOURCE_PATH']))


    def artist(self, scene, n_samples, spectrum, output_name):
        subprocess.Popen(
            ['artist', scene,
            '-DSAMPLES='+str(n_samples),
            '-DSPECTRUM=' + str(spectrum),
            '-seed', str(self.seed),
            '-o', output_name,
            '-b'
            ],
            env = self.env
        )


    def bugblatter(self, reference, comparison, bugblatter_max, output):
        subprocess.Popen(
            ['bugblatter', reference, comparison,
            '-m', str(bugblatter_max),
            '-r',
            '-o', output,
            '-b'
            ],
            env = self.env
        )


    def imgsnr(self, reference, comparison, output):
        subprocess.call(
            ['art_imagesnr', reference,
            '-c', comparison,
            '-o', output
            ],
            env = self.env
        )