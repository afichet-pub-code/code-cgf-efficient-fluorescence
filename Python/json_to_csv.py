#!/usr/bin/env python3

from genericpath import isfile
from multiprocessing.spawn import is_forking
import os, sys, json


def main(json_fit, csv_gaussian, csv_diagonal):
    fits = ['em', 'bayes']
    max_gaussians = 8
    
    gaussian_headers_common = [
        'Material name',
        'Fitting technique',
        'Number of Gaussians',
        'Effective number of Gaussians',
        'Scale attenuation'
    ]

    gaussian_headers_gaussian = [
        'Gaussian #{} weight',
        'Gaussian #{} mean x',
        'Gaussian #{} mean y',
        'Gaussian #{} covariance[0][0]',
        'Gaussian #{} covariance[0][1]',
        'Gaussian #{} covariance[1][0]',
        'Gaussian #{} covariance[1][1]']
    
    with open(json_fit) as json_in:
        json_fit = json.load(json_in)

        # Output fitted Gaussian parameters
        with open(csv_gaussian, 'w') as csv_gauss_out:
            gaussian_headers = gaussian_headers_common
            gaussian_headers += [
                h.format(i + 1)
                for i in range(max_gaussians)
                for h in gaussian_headers_gaussian
                ]

            csv_gauss_out.write(', '.join(gaussian_headers) + '\n')
            
            for mat in json_fit:
                mat_iter = 0

                for fit in fits:            
                    fit_iter = 0
                    
                    for n_gauss in json_fit[mat][fit]:
                        mat_field = mat if mat_iter == 0 else ''
                        fit_field = fit if fit_iter == 0 else ''
                        
                        mat_iter += 1
                        fit_iter += 1
                    
                        f_list = [ json_fit[mat][fit][n_gauss]['scaling_factor'] ]

                        # The number of Gaussians can be lower than the target
                        effective_n_gauss = len(json_fit[mat][fit][n_gauss]['weights'])
                    
                        for i in range(effective_n_gauss):
                            f_list += [ json_fit[mat][fit][n_gauss]['weights'][i] ]
                            f_list += json_fit[mat][fit][n_gauss]['means'][i]
                            f_list += json_fit[mat][fit][n_gauss]['covs'][i]
                        
                        str_list = [ mat_field, fit_field, n_gauss, str(effective_n_gauss) ]
                        str_list += [ str(f) for f in f_list ]
                        
                        csv_gauss_out.write(', '.join(str_list) + '\n')

        # Output the diagonal
        with open(csv_diagonal, 'w') as csv_diag_out:
            m0 = next(iter(json_fit))
            n_wl     = len(json_fit[m0]['diagonal']['data'])
            step_wl  = json_fit[m0]['diagonal']['step_wl']
            start_wl = json_fit[m0]['diagonal']['start_wl']
            end_wl   = start_wl + (n_wl - 1) * step_wl

            diagonal_headers = [ 'Material name' ]
            diagonal_headers += [ str(i) for i in range(start_wl, end_wl + step_wl, step_wl) ]
            
            csv_diag_out.write(', '.join(diagonal_headers) + '\n')

            for mat in json_fit:
                # Check if the json data match our common header
                json_n_wl     = len(json_fit[mat]['diagonal']['data'])
                json_step_wl  = json_fit[mat]['diagonal']['step_wl']
                json_start_wl = json_fit[mat]['diagonal']['start_wl']
                json_end_wl   = json_start_wl + (json_n_wl - 1) * json_step_wl

                if (start_wl != json_start_wl
                    or step_wl != json_step_wl
                    or end_wl != json_end_wl):
                    print('Ignoring material "{}": not the same diagonal sampling'.format(mat))
                    print('Expecting: start: {}nm | end: {}nm | step: {}nm step'.format(
                        start_wl, end_wl, step_wl))
                    print('Got:       start: {}nm | end: {}nm | step: {}nm step'.format(
                        json_start_wl, json_end_wl, json_step_wl))
                else:
                    line = [ mat ]
                    line += [ str(i) for i in json_fit[mat]['diagonal']['data'] ]

                    csv_diag_out.write(', '.join(line) + '\n')

            
if __name__ == '__main__':
    json_in           = 'fits.json'
    csv_out_gaussians = 'gaussian_params.csv'
    csv_out_diagonals = 'diagonals.csv'
    
    if len(sys.argv) >= 4:
        json_in           = sys.argv[1]
        csv_out_gaussians = sys.argv[2]
        csv_out_diagonals = sys.argv[3]
    elif len(sys.argv) >= 2:
        json_in           = sys.argv[1]

    if not os.path.isfile(json_in):
        print('Error: the Json file "{}" does not exist'.format(json_in))
        exit(1)

    main(json_in, csv_out_gaussians, csv_out_diagonals)
