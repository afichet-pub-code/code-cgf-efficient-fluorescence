#!/usr/bin/env python3

import os, tempfile
from tools.art import ART
from tools.gnuplot import Gnuplot
import numpy as np

from pipeline.filtering import basic_fourier_filter 
from radiometry.fluospectrum import FluoSpectrum
from radiometry.cmf import CMF, XYZ_to_sRGB, to_sRGB_gamma

current_dir = os.path.dirname(os.path.realpath(__file__))
output_directory = os.path.join(current_dir, '..', 'output')
path_dataset = os.path.join(current_dir, '..', 'data', 'RIT')


def add_sample_html(name, path_reradiation_png, path_reflectance_png, path_abs_reem_png):
    return """
    <h1>{name}</h1>

    <table>
        <tr>
            <th>Reradiation</th>
            <th>Non-fluo reflectance</th>
            <th>Absorption and reemission</th>
        </tr>
        <tr>
            <td><img src="{path_reradiation_png}" /></td>
            <td><img src="{path_reflectance_png}" /></td>
            <td><img src="{path_abs_reem_png}" /></td>
        </tr>
    </table>
    """.format(
        name=name, 
        path_reradiation_png=path_reradiation_png,
        path_reflectance_png=path_reflectance_png,
        path_abs_reem_png=path_abs_reem_png)


def add_section_tex(name):
    return "\\section{{{}}}".format(name)


def add_subsection_tex(name):
    return "\\subsection{{{}}}".format(name)


def add_sample_tex(path_reradiation, path_reflectance, path_abs_reem):
    return """
    \\begin{{center}}
        \\input{{{path_reflectance}}}%
        \\input{{{path_abs_reem}}}%
        \\input{{{path_reradiation}}}
    \\end{{center}}
    """.format(
        path_reradiation=path_reradiation,
        path_reflectance=path_reflectance,
        path_abs_reem=path_abs_reem
    )


if __name__ == '__main__':
    art = ART()
    gp = Gnuplot()

    #bfc_files = [f for f in os.listdir(path_dataset) if os.path.isfile(os.path.join(path_dataset, f)) and f.endswith('.BFC')]
    bfc_files = []

    for dirpath, dirname, filenames in os.walk(path_dataset):
        for file in filenames:
            if file.endswith('.BFC'):
                bfc_files.append(os.path.join(dirpath, file)[len(path_dataset) + 1:])

    print('Found: {} BFC dataset(s)'.format(len(bfc_files)))

    # Create output directory if doesn't exists
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    # html_body = ""
    latex_body = ""

    # Sort the dataset by fluorescence
    data = []

    for f in bfc_files:
        # Load dataset
        fluo_spectrum = FluoSpectrum(os.path.join(path_dataset, f))
        sum_refl = np.sum(fluo_spectrum.get_average_reemission_spectrum())
        data.append([f, sum_refl])

    data.sort(key = lambda x: x[1], reverse=True)
    bfc_files = np.array(data)

    histogram_dataset_dat = os.path.join(output_directory, 'histogram.dat')
    histogram_dataset_tex = os.path.join(output_directory, 'histogram.tex')

    with open(histogram_dataset_dat, 'w') as f:
        # for file, fluo_power in bfc_files:
        for file, fluo_power in bfc_files[1:,:]:
            f.write('{} {}\n'.format(file, fluo_power))

    gp.plot_histogram_summary_tex(histogram_dataset_dat, histogram_dataset_tex)

    # html_body += '<img src="{}" />'.format(histogram_dataset_tex)

    # For each file, show reradiation, non fluo reflectance and average
    # absorption and reemission spectra
    for file in bfc_files[:, 0]:
        file_name = file.replace('/', '-').replace('\\', '-')[:-4]
        latex_body += add_section_tex(file_name)

        latex_body += add_subsection_tex("Original")

        fluo_spectrum = FluoSpectrum(os.path.join(path_dataset, file))

        output_matrix_rerad_mat = os.path.join(output_directory, file_name + '-mat.dat')
        output_matrix_rerad_tex = os.path.join(output_directory, file_name + '-mat.tex')

        output_nonfluo_dat = os.path.join(output_directory, file_name + '-nonfluo.dat')
        output_nonfluo_tex = os.path.join(output_directory, file_name + '-nonfluo.tex')

        output_absorption_dat = os.path.join(output_directory, file_name + '-absorb.dat')
        output_reemission_dat = os.path.join(output_directory, file_name + '-reemit.dat')
        output_abs_reem_tex   = os.path.join(output_directory, file_name + '-absorb-reemit.tex')

        fluo_spectrum.gnuplot_reradiation(output_matrix_rerad_mat)
        fluo_spectrum.gnuplot_non_fluo(output_nonfluo_dat)
        fluo_spectrum.gnuplot_average_absorption(output_absorption_dat)
        fluo_spectrum.gnuplot_average_reemission(output_reemission_dat)

        gp.plot_reradiation_tex(output_matrix_rerad_mat, output_matrix_rerad_tex)
        gp.plot_spectrum_tex(output_nonfluo_dat, output_nonfluo_tex)
        gp.plot_absorption_reemission_tex(output_absorption_dat, output_reemission_dat, output_abs_reem_tex)
        
        # html_body += add_sample(file_name, output_matrix_rerad_tex, output_nonfluo_tex, output_abs_reem_tex)
        latex_body += add_sample_tex(output_matrix_rerad_tex, output_nonfluo_tex, output_abs_reem_tex)


        # Apply filtering

        latex_body += add_subsection_tex("Filtered")

        fluo_spectrum = basic_fourier_filter(fluo_spectrum)

        output_matrix_rerad_mat = os.path.join(output_directory, file_name + '_mat.dat')
        output_matrix_rerad_tex = os.path.join(output_directory, file_name + '_mat.tex')

        output_nonfluo_dat = os.path.join(output_directory, file_name + '_nonfluo.dat')
        output_nonfluo_tex = os.path.join(output_directory, file_name + '_nonfluo.tex')

        output_absorption_dat = os.path.join(output_directory, file_name + '_absorb.dat')
        output_reemission_dat = os.path.join(output_directory, file_name + '_reemit.dat')
        output_abs_reem_tex   = os.path.join(output_directory, file_name + '_absorb_reemit.tex')

        fluo_spectrum.gnuplot_reradiation(output_matrix_rerad_mat)
        fluo_spectrum.gnuplot_non_fluo(output_nonfluo_dat)
        fluo_spectrum.gnuplot_average_absorption(output_absorption_dat)
        fluo_spectrum.gnuplot_average_reemission(output_reemission_dat)

        gp.plot_reradiation_tex(output_matrix_rerad_mat, output_matrix_rerad_tex)
        gp.plot_spectrum_tex(output_nonfluo_dat, output_nonfluo_tex)
        gp.plot_absorption_reemission_tex(output_absorption_dat, output_reemission_dat, output_abs_reem_tex)
        
        # html_body += add_sample(file_name, output_matrix_rerad_png, output_nonfluo_png, output_abs_reem_png)
        latex_body += add_sample_tex(output_matrix_rerad_tex, output_nonfluo_tex, output_abs_reem_tex)


    # with open(os.path.join(output_directory, "index.html"), "w") as f:
    #     f.write("""
    #         <html>
    #             <head>
    #                 <title>Report</title>
    #             </head>
    #             <body>
    #             {}
    #             </body>
    #         </html>
    #     """.format(html_body)
    #     )

    with open(os.path.join(output_directory, "main_2.tex"), "w") as f:
        f.write("""
            \\documentclass{{article}}

            \\usepackage{{microtype}}
            \\usepackage{{geometry}}
            \\usepackage{{color}}
            \\usepackage{{graphicx}}
            \\usepackage{{tabularx}}
            \\usepackage{{fullpage}}

            \\usepackage{{tikz}}
            \\usepackage{{pgfplots, pgfplotstable}}

            \\title{{Summary BFC measurements}}

            \\begin{{document}}
            {}
            \\end{{document}}
        """.format(latex_body)
        )
