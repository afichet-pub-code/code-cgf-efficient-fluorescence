#!/usr/bin/env python3

import os
from radiometry.fluospectrum import FluoSpectrum

def find_bfc_files(path_dataset):
    bfc_files = [
        os.path.join(path_dataset, f)
        for f in os.listdir(path_dataset)
        if os.path.isfile(os.path.join(path_dataset, f)) and f.endswith(".BFC")
    ]

    print("Found: {} BFC dataset(s)".format(len(bfc_files)))
    
    return bfc_files

# Load and sort dataset by fluorescence
def load_spectra(files):
    import numpy as np
    loaded_spectra = []

    for f in files:
        # Load dataset
        fluo_spectrum = FluoSpectrum(f)
        sum_refl = np.sum(fluo_spectrum.get_average_reemission_spectrum())
        # sum_refl = np.sum(fluo_spectrum.data)
        # sum_refl = np.sum(fluo_spectrum.get_non_fluo())
        loaded_spectra.append([fluo_spectrum, sum_refl])

    loaded_spectra.sort(key = lambda x: x[1], reverse=True)
    loaded_spectra = np.array(loaded_spectra)[:,0]
    
    return loaded_spectra
