import copy
import numpy as np

from radiometry.fluospectrum import FluoSpectrum

# Zeroes out value <= threshold
def zero_out_threshold(data, threshold):
    for o in range(data.shape[0]):
        for i in range(data.shape[1]):
            if data[o, i] < threshold:
                data[o, i] = 0
    return data


# Zeroes out values under the diagonal
def zero_out_non_fluo(fluo_spectrum, data):
    for o in range(data.shape[0]):
        for i in range(data.shape[1]):
            if fluo_spectrum.wl_for_idx_in(i) >= fluo_spectrum.wl_for_idx_out(o):
                data[o, i] = 0
    return data


# Zeroes out values under the diagonal and values under the threshold used in the EGSR publication
def zero_out_invalid_threshold_egsr(fluo_spectrum, data):
    zero_out_threshold(data, 0.006)
    zero_out_non_fluo(fluo_spectrum, data)
    return data


# Zeroes out values under the diagonal and negative values
def zero_out_invalid_threshold_zero(fluo_spectrum, data):
    zero_out_threshold(data, 0)
    zero_out_non_fluo(fluo_spectrum, data)
    return data


# Zeroes out values under the diagonal and rest of the values based on
# maximal "possible" measurement noise (absolute value of the lowest value)
def zero_out_invalid_threshold_max_noise(fluo_spectrum, data):
    zero_out_threshold(data, abs(np.min(data)))
    zero_out_non_fluo(fluo_spectrum, data)
    return data


# Box filtering in fourier domain
def filter_fourier_box(fluo_spectrum, sqr_radius=60):
    fluo = fluo_spectrum.get_pure_fluo()
    fluo_valid = zero_out_invalid_threshold_zero(fluo_spectrum, fluo)

    fourier = np.fft.rfft2(fluo_valid, fluo_valid.shape)
    fourier_filtered = copy.deepcopy(fourier)

    for x in range(0, fourier.shape[0]):
        for y in range(0, fourier.shape[1]):
            if (x * x + y * y) > sqr_radius and (
                (fourier.shape[0] - x) ** 2 + y ** 2
            ) > sqr_radius:
                fourier_filtered[x, y] = 0
            else:
                fourier_filtered[x, y] = fourier[x, y]

    fluo_filtered = np.fft.irfft2(fourier_filtered, fluo_valid.shape)
    fluo_filtered_valid = zero_out_invalid_threshold_zero(fluo_spectrum, fluo_filtered)

    return fluo_filtered_valid
