import numpy as np
from radiometry.fluospectrum import FluoSpectrum
from radiometry.spectrum import Spectrum, TabSpectrum

class CMF:
    def __init__(self, filename):
        x_bar = []
        y_bar = []
        z_bar = []
        
        # Read the CSV file
        with open(filename) as f:    
            for l in f:
                wl, x, y, z = [float(el) for el in l.split(',')]
                x_bar.append([wl, x])
                y_bar.append([wl, y])
                z_bar.append([wl, z])

        x_bar = np.array(x_bar)
        y_bar = np.array(y_bar)
        z_bar = np.array(z_bar)
        
        # Interpolate every 1nm to ease computations
        self.wl_start = np.min(x_bar[:, 0])
        self.wl_end   = np.max(x_bar[:, 0])
        self.wl       = np.linspace(self.wl_start, self.wl_end, num=int(self.wl_end - self.wl_start + 1))

        x_bar_y = np.interp(self.wl, x_bar[:, 0], x_bar[:, 1], left=0, right=0)
        y_bar_y = np.interp(self.wl, y_bar[:, 0], y_bar[:, 1], left=0, right=0)
        z_bar_y = np.interp(self.wl, z_bar[:, 0], z_bar[:, 1], left=0, right=0)

        self.x_bar = np.stack((self.wl, x_bar_y), axis=1)
        self.y_bar = np.stack((self.wl, y_bar_y), axis=1)
        self.z_bar = np.stack((self.wl, z_bar_y), axis=1)
        
        
    # Get XYZ values for a given emissive spectrum
    def get_xyz_emissive(self, spectrum: Spectrum):
        # special case, mono
        if spectrum.data.shape[0] == 1:
            mono_wl = spectrum.data[0, 0]
            
            if mono_wl < self.wl_start or spectrum.data[0, 0] > self.wl_end:
                return 0, 0, 0
            else:
                idx_low  = int(np.floor(mono_wl) - self.wl_start)
                idx_high = idx_low + 1
                interp = (mono_wl - self.wl[idx_low])
                x_bar_interp = 0
                y_bar_interp = 0
                z_bar_interp = 0

                if idx_low >= 0:
                    x_bar_interp += (1 - interp) * self.x_bar[idx_low, 1]
                    y_bar_interp += (1 - interp) * self.y_bar[idx_low, 1]
                    z_bar_interp += (1 - interp) * self.z_bar[idx_low, 1]
                if idx_high <= self.x_bar.shape[0]:
                    x_bar_interp += interp * self.x_bar[idx_high, 1]
                    y_bar_interp += interp * self.y_bar[idx_high, 1]
                    z_bar_interp += interp * self.z_bar[idx_high, 1]
                
                value = spectrum.data[0, 1]
                
                return x_bar_interp * value, y_bar_interp * value, z_bar_interp * value
                    
        # Interpolate spectrum to match the CMFs datapoints
        spectrum_values = np.interp(self.wl, spectrum.data[:, 0], spectrum.data[:, 1], left=0, right=0)
        
        spectrum_x = np.trapz([ x_b * s for x_b, s in zip(self.x_bar[:, 1], spectrum_values) ], self.wl)
        spectrum_y = np.trapz([ y_b * s for y_b, s in zip(self.y_bar[:, 1], spectrum_values) ], self.wl)
        spectrum_z = np.trapz([ z_b * s for z_b, s in zip(self.z_bar[:, 1], spectrum_values) ], self.wl)
    
        return spectrum_x, spectrum_y, spectrum_z
    
    
    # Get XYZ values for a given reflectance spectrum under a given illuminant
    def get_xyz_reflective(self, spectrum: Spectrum, illuminant: Spectrum):
        # Warning, this is untested...
        # special case, mono
        if spectrum.data.shape[0] == 1 and illuminant.data.shape[0]:
            if spectrum.data[0, 0] != illuminant.data[0, 0]:
                return 0, 0, 0
            
            mono_wl = spectrum.data[0, 0]
            
            if mono_wl < self.wl_start or spectrum.data[0, 0] > self.wl_end:
                return 0, 0, 0
            else:
                idx_low  = int(np.floor(mono_wl) - self.wl_start)
                idx_high = idx_low + 1
                interp = (mono_wl - self.wl[idx_low])
                x_bar_interp = 0
                y_bar_interp = 0
                z_bar_interp = 0

                if idx_low >= 0:
                    x_bar_interp += (1 - interp) * self.x_bar[idx_low, 1]
                    y_bar_interp += (1 - interp) * self.y_bar[idx_low, 1]
                    z_bar_interp += (1 - interp) * self.z_bar[idx_low, 1]
                if idx_high <= self.x_bar.shape[0]:
                    x_bar_interp += interp * self.x_bar[idx_high, 1]
                    y_bar_interp += interp * self.y_bar[idx_high, 1]
                    z_bar_interp += interp * self.z_bar[idx_high, 1]
                
                refl  = spectrum.data[0, 1]
                illu = illuminant.data[0, 1]
                
                return x_bar_interp * illu * value, y_bar_interp * illu * value, z_bar_interp * illu * value
        
        # Interpolate spectrum to match the CMFs datapoints
        spectrum_values   = np.interp(self.wl, spectrum.data  [:, 0], spectrum.data  [:, 1], left=0, right=0)
        illuminant_values = np.interp(self.wl, illuminant.data[:, 0], illuminant.data[:, 1], left=0, right=0)
                
        spectrum_x = np.trapz([ x_b * refl * illu for x_b, refl, illu in zip(self.x_bar[:, 1], spectrum_values, illuminant_values) ], self.wl)
        spectrum_y = np.trapz([ y_b * refl * illu for y_b, refl, illu in zip(self.y_bar[:, 1], spectrum_values, illuminant_values) ], self.wl)
        spectrum_z = np.trapz([ z_b * refl * illu for z_b, refl, illu in zip(self.z_bar[:, 1], spectrum_values, illuminant_values) ], self.wl)
    
        return spectrum_x, spectrum_y, spectrum_z
    
    
    # Get XYZ value for a given reflectance bispectral spectrum under a given illuminant
    def get_xyz_reflective_fluo(self, fluo_spectrum: FluoSpectrum, illuminant: Spectrum):
        pure_fluo = fluo_spectrum.get_pure_fluo_filtered()

        wl_i = fluo_spectrum.get_excitation_wavelengths()
        wl_o = fluo_spectrum.get_reflectance_wavelengths()

        illu_interp = np.interp(wl_i, illuminant.data[:, 0], illuminant.data[:, 1], left=0, right=0)

        wl_i_interp = np.arange(fluo_spectrum.wavelength_i_start, fluo_spectrum.wavelength_i_end + 1)
        wl_o_interp = np.arange(fluo_spectrum.wavelength_o_start, fluo_spectrum.wavelength_o_end + 1)

        out_spectrum = np.matmul(pure_fluo, illu_interp)
        xyz_fluo = self.get_xyz_emissive(TabSpectrum(wl_o, out_spectrum))

        # Get reflectance for the diagonal
        xyz_diag = self.get_xyz_reflective(fluo_spectrum.get_non_fluo_spectrum(), illuminant)
        xyz_ret = [ diag + fluo for diag, fluo in zip(xyz_diag, xyz_fluo) ]
        
        return xyz_ret
    
    
def XYZ_to_sRGB(xyz):
    mat_sRGB = [[ 3.2404542, -1.5371385, -0.4985314],
                [-0.9692660, 1.8760108, 0.0415560],
                [0.0556434, -0.2040259, 1.0572252]]
    rgb = np.matmul(mat_sRGB, xyz)
    
    return rgb


def to_sRGB_gamma(C):
    C = np.clip(C, 0., 1.)
    
    if abs(C) < 0.0031308:
        return 12.92 * C
    return 1.055 * C**0.41666 - 0.055