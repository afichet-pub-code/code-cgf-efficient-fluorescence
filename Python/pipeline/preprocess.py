import numpy as np
import collections
# Collection of different pre-process algorithms.
# Work in progress
# For now the m2 should work best.
def make_training_set_m1(fluo_spectrum, sample_size = 3000):
    x = fluo_spectrum.get_excitation_wavelengths()
    y = fluo_spectrum.get_reflectance_wavelengths()
    X, Y = np.meshgrid(x, y)

    X = np.ravel(X)
    Y = np.ravel(Y)
    reradiation = np.ravel(fluo_spectrum.get_pure_fluo())
    

    reradiation[reradiation < 0] = 0
    rerad_norm = reradiation / np.sum(reradiation)
    
    idx = np.random.choice(len(rerad_norm), size=sample_size, replace=True, p=rerad_norm)
    selected_x = [X[i] for i in idx]
    selected_y = [Y[i] for i in idx]
    training_set = np.vstack([selected_x,selected_y]).T

    return training_set


def make_training_set_m3(fluo_spectrum, scale = 100.0):
    x = fluo_spectrum.get_excitation_wavelengths()
    y = fluo_spectrum.get_reflectance_wavelengths()
    z = np.ravel(fluo_spectrum.get_pure_fluo())
    z[z <=0.0] = 0.0
    full_rerad_sum = np.sum(z)

    training_set = []
    for wl_i in fluo_spectrum.get_excitation_wavelengths():
        wl_i_id = (wl_i - fluo_spectrum.wavelength_i_start) / fluo_spectrum.wavelength_i_sampling
            
        reradation = fluo_spectrum.get_pure_fluo()[:, int(wl_i_id)]
        reradation[reradation <=0.0] = 0.0
        #if(np.sum(reradation) / full_rerad_sum <0.1):
        #    continue
        rerad_norm = reradation / np.sum(reradation)
        n_samples = scale*np.sum(reradation)
        idx = np.random.choice(len(rerad_norm), size=int(n_samples), replace=True, p=rerad_norm)

        selected_y = [y[i] for i in idx]

        selected_x = np.zeros(np.shape(selected_y))
        selected_x.fill(wl_i)

        p = np.vstack([selected_x,selected_y])
        training_set.append(p.T)

    training_set = np.concatenate(training_set)

    return training_set


def make_training_set_m2(fluo_spectrum, baseline = 0.0000, repeats = 1000):
    x = fluo_spectrum.get_excitation_wavelengths()
    y = fluo_spectrum.get_reflectance_wavelengths()
    X, Y = np.meshgrid(x, y)

    X = np.ravel(X)
    Y = np.ravel(Y)
    Z = np.ravel(fluo_spectrum.get_pure_fluo())
    training_set = np.vstack([X,Y,Z]).T
        
    # Clean the data set, cut the probability that is lower than the baseline.
    #Z[Z <=baseline] = 0.0

    # works as alternative
    training_set = training_set[training_set[:,2] > baseline]
    Z = Z[Z > baseline]
 
    # Use the knowledge of z-axis(probability) to clean the data.
    # Generate more data points based on z value.
    # The original z value is small, so we simply scale it.

    Z *= repeats
    Z = np.floor(Z)
    Z = Z.astype(int)

    points = []
    clean = []
    for i in range(len(training_set)):
        # Replicate the data. 
        # By all means, the probabily only makes sense 
        # when it's repeating time is bigger than one.
        if Z[i] >= 1:
            for j in range(Z[i]-1):
                p = training_set[i]
                points.append(p)
                
    # Pack new data into training set.
    training_set = np.vstack(np.array(points))
    # Use 2d data in GMM to reduce running complexity.
    # The z-axis is not needed for the result in x-y plane.
    training_set_xy = np.array([training_set[:,0],training_set[:,1]]).T

    return training_set_xy