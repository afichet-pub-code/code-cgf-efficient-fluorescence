from scipy.stats import multivariate_normal
import struct

import numpy as np
from scipy.optimize import least_squares
import sklearn.metrics as metrics
from radiometry.fluospectrum import FluoSpectrum, RSFluoSpectrum

def optim(x, *args, **kwargs):
    """Optimiziation process by MSE"""
    scaling = 1
    for i in kwargs['original'].shape:
        scaling *= i

    res = scaling * metrics.mean_squared_error(kwargs['original'], abs(x[0]) * kwargs['fit'])

    return res


def optim_weighted(x, *args, **kwargs):
    """Optimiziation process by weighted MSE"""
    scaling = 1
    for i in kwargs['original'].shape:
        scaling *= i

    return scaling * metrics.mean_squared_error(kwargs['original'], abs(x[0]) * kwargs['fit'],
            sample_weight=kwargs['weights'])


def optim_L1(x, *args, **kwargs):
    """Optimiziation process by L1 loss function(Mean Absolute Error)"""
    scaling = 1
    for i in kwargs['original'].shape:
        scaling *= i

    return  scaling * metrics.mean_absolute_error(kwargs['original'], abs(x[0]) * kwargs['fit'])
            

class GMM:
    """
    Represents a Gaussian Mixture Model.

    This class represents a fluorescent spectrum as a Gaussian Mixture Model
    (GMM). It is responsible of loading and writing files serializing the
    various parameters of the mixture. It does not perfom the fitting.
    """

    def __init__(self, filename: str):
        """
        Initialize a Gaussian Mixture Model from a file.

        :param filename : The path of the file to load.
        """
        self.means        = []
        self.covs         = []
        self.mixing_coefs = []
        self.bic          = 0.0
        self.scale_attenuation = 1
        self.start_wl, self.step_wl = 0, 0
        self.diagonal     = []

        with open(filename, 'rb') as f:
            data = f.read()
            offset = 0

            # Gaussian mixture
            # ----------------

            unpack_str  = '=I'
            n_gaussians = struct.unpack_from(unpack_str, data, offset)[0]
            offset += struct.calcsize(unpack_str)

            # means
            unpack_str = '=' + str(2*n_gaussians) + 'd'
            gaussian_means = struct.unpack_from(unpack_str, data, offset)
            offset += struct.calcsize(unpack_str)

            # covariances
            unpack_str = '=' + str(3*n_gaussians) + 'd'
            gaussian_covs = struct.unpack_from(unpack_str, data, offset)
            offset += struct.calcsize(unpack_str)

            # mixing coef
            unpack_str = '=' + str(n_gaussians) + 'd'
            self.mixing_coefs = struct.unpack_from(unpack_str, data, offset)
            offset += struct.calcsize(unpack_str)

            for i in range(n_gaussians):
                self.means.append(np.array(
                    [gaussian_means[2*i + 0], 
                    gaussian_means[2*i + 1]]))

                self.covs.append(np.array(
                    [[gaussian_covs[3*i + 0], gaussian_covs[3*i + 1]], 
                     [gaussian_covs[3*i + 1], gaussian_covs[3*i + 2]]]))


            self.means = np.reshape(self.means, (n_gaussians, 2))

            # scale_attenuation
            unpack_str = '=d'
            self.scale_attenuation = struct.unpack_from(unpack_str, data, offset)[0]
            offset += struct.calcsize(unpack_str)

            # Diagonal
            # --------

            unpack_str = '=Idd'
            size_diagonal, self.start_wl, self.step_wl = struct.unpack_from(unpack_str, data, offset)
            offset += struct.calcsize(unpack_str)

            unpack_str = '=' + str(size_diagonal) + 'd'
            self.diagonal = struct.unpack_from(unpack_str, data, offset)


    def recompute_weight(self, fluo_spectrum, method):
        print('Old weight: {}'.format(self.scale_attenuation))
        self.scale_attenuation = self.fit_weight(fluo_spectrum, method)
        print('New weight: {}'.format(self.scale_attenuation))

    # save binary file
    def save(self, output: str) -> None:
        """
        Save to the native binary format readable by ``__init``.

        Parameters
        ----------
        output : string
            Path to the file to save.
        """
        n_gaussians = len(self.means)
        size_diagonal = len(self.diagonal)

        # prepare data for export
        means = []
        covs = []

        for m, c in zip(self.means, self.covs):
            means.append(m[0])
            means.append(m[1])

            covs.append(c[0, 0])
            covs.append(c[0, 1])
            covs.append(c[1, 1])

        with open(output, 'wb') as f:
            b = struct.pack(
                '=I'                        # n_gaussians
                + str(2*n_gaussians) + 'd'  # means
                + str(3*n_gaussians) + 'd'  # covariances
                + str(n_gaussians) + 'd'    # mixing coef
                + 'd'                       # scale_attenuation
                + 'Idd'                     # size_diagonal, start_diagonal, step_diagonal
                + str(size_diagonal) + 'd', # diagonal
                n_gaussians,
                *means, *covs, *self.mixing_coefs,
                self.scale_attenuation,
                size_diagonal, self.start_wl, self.step_wl,
                *self.diagonal
                )

            f.write(b)


    def save_ascii(self, output: str) -> None:
        """ 
        Saves the parameters of the mixture in an ASCII file.

        This method allows to write to a plain text file the parameters of the
        fit. The created file will not be readable by ``__init__``. Its purpose
        is to allow easy reading of the computed value from a standard text
        editor.

        Parameters
        ----------
        output : string
            Path to the file to save.
        """
        with open(output, 'w') as f:
            f.write('gmm\n')
            f.write('gaussians:\n')
            f.write('{}\n'.format(len(self.mixing_coefs)))

            f.write('means:\n')
            for mean in self.means:
                f.write('{}, {}\n'.format(mean[0], mean[1]))

            f.write('covs:\n')
            for cov in self.covs:
                f.write('{}, {}, {}, {}\n'.format(cov[0,0], cov[0, 1], cov[1, 0], cov[1, 1]))

            f.write('weights:\n')
            for weigth in self.mixing_coefs:
                f.write('{}\n'.format(weigth))

            f.write('diagonal:\n')
            f.write('{}, {}, {}\n'.format(len(self.diagonal), self.start_wl, self.step_wl))
            for v in self.diagonal:
                f.write('{}\n'.format(v))

            f.write('scaling_factor:\n')
            f.write('{}\n'.format(self.scale_attenuation))     
            

    def print_params(self) -> None:
        """
        Prints the mixture parameters.
        """
        for mean, cov, weight in zip(self.means, self.covs, self.mixing_coefs):
            print(mean, cov, weight)

    def get_num_components(self) -> int:
        return len(self.means)

    def get_fluospectrum(
        self) -> RSFluoSpectrum:
        # TODO
        # This is hardcoded stuff... really shall not be used outside the provided datasets!

        wl_i_values = np.linspace(300, 780, 49)
        wl_o_values = np.linspace(380, 780, 41)

        reradiation = np.zeros((41, 49))

        for wl_o, o in zip(wl_o_values, range(len(wl_o_values))):
            for wl_i, i in zip(wl_i_values, range(len(wl_i_values))):
                if wl_o > wl_i:
                    reradiation[o, i] = self.eval_attenuation(wl_i, wl_o)
                elif wl_o == wl_i:
                    diag_idx = int((wl_i - self.start_wl) // self.step_wl)
                    reradiation[o, i] = self.diagonal[diag_idx]

        return RSFluoSpectrum(300, 10, 380, 10, reradiation)


    def get_pure_fluo(
        self, 
        start_wl_i: float = 300., end_wl_i:float = 780., n_samples_i: int = 49,
        start_wl_o: float = 380., end_wl_o:float = 780., n_samples_o: int = 41) -> np.array:
        """
        Reconstruct the fluorescent attenuation.

        Parameters
        ----------
        start_wl_i : float, optional
            Starting radiating wavelength in nanometers.
        end_wl_i : float, optional 
            Ending radiating wavelength in nanometers.
        n_samples_i : integer, optional
            Number of radiating wavelegths samples.
        start_wl_o : float, optional 
            Starting reemitting wavelength in nanometers.
        end_wl_o : float, optional
            Ending reemitting wavelength in nanometers.
        n_samples_o : integer, optional
            Number of reemitting wavelegths samples.

        Returns
        -------
        A matrix of the pure fluorescent reradiation.
        """
        if n_samples_i <= 1 or n_samples_o <= 1:
            raise Exception("Cannot use less than two samples")

        wl_i_values = np.linspace(start_wl_i, end_wl_i, n_samples_i)
        wl_o_values = np.linspace(start_wl_o, end_wl_o, n_samples_o)

        reradiation = np.zeros((n_samples_o, n_samples_i))

        for wl_o, o in zip(wl_o_values, range(len(wl_o_values))):
            for wl_i, i in zip(wl_i_values, range(len(wl_i_values))):
                reradiation[o, i] = self.eval_attenuation(wl_i, wl_o) if wl_o > wl_i else 0

        return reradiation


    def gnuplot_reradiation(
        self, 
        output: str, 
        start_wl_i: float = 300., end_wl_i: float = 780., n_samples_i: int = 49,
        start_wl_o: float = 380., end_wl_o: float = 780., n_samples_o: int = 41) -> None:
        """
        Export the reradiation in a Gnuplot readable format.

        Parameters
        ----------
        start_wl_i : float, optional
            Starting radiating wavelength in nanometers.
        end_wl_i : float, optional 
            Ending radiating wavelength in nanometers.
        n_samples_i : integer, optional
            Number of radiating wavelegths samples.
        start_wl_o : float, optional 
            Starting reemitting wavelength in nanometers.
        end_wl_o : float, optional
            Ending reemitting wavelength in nanometers.
        n_samples_o : integer, optional
            Number of reemitting wavelegths samples.
        """

        if n_samples_i <= 1 or n_samples_o <= 1:
            raise Exception("Cannot use less than two samples")

        wl_i_values = np.linspace(start_wl_i, end_wl_i, n_samples_i)
        wl_o_values = np.linspace(start_wl_o, end_wl_o, n_samples_o)

        with open(output, 'w') as f:
            for wl_o in wl_o_values:
                for wl_i in wl_i_values:
                    f.write('{} '.format(self.eval_attenuation(wl_i, wl_o) if wl_o > wl_i else 0))

                f.write('\n')


    def tikz_reradiation(
        self, 
        output: str,
        start_wl_i: float = 300., end_wl_i: float = 780., n_samples_i: int = 49,
        start_wl_o: float = 380., end_wl_o: float = 780., n_samples_o: int = 41) -> None:
        """
        Export the reradiation in a tikz readable format.

        Parameters
        ----------
        start_wl_i : float, optional
            Starting radiating wavelength in nanometers.
        end_wl_i : float, optional 
            Ending radiating wavelength in nanometers.
        n_samples_i : integer, optional
            Number of radiating wavelegths samples.
        start_wl_o : float, optional 
            Starting reemitting wavelength in nanometers.
        end_wl_o : float, optional
            Ending reemitting wavelength in nanometers.
        n_samples_o : integer, optional
            Number of reemitting wavelegths samples.
        """

        if n_samples_i <= 1 or n_samples_o <= 1:
            raise Exception("Cannot use less than two samples")

        wl_i_values = np.linspace(start_wl_i, end_wl_i, n_samples_i)
        wl_o_values = np.linspace(start_wl_o, end_wl_o, n_samples_o)

        with open(output, 'w') as f:
            for wl_o in wl_o_values:
                for wl_i in wl_i_values:

                    f.write('{} {} {}\n'.format(wl_i, wl_o, self.eval_attenuation(wl_i, wl_o) if wl_o > wl_i else 0))

                f.write('\n')


    def eval_gmm(self, wl_i: float, wl_o: float) -> float:
        """
        Gets the GMM value for specific radiating and reemitting wavelengths.

        Parameters
        ----------
        wl_i : float
            Radiating wavelength in nanometers.
        wl_o : float
            Reemitting wavelenght in nanometers.

        Returns
        -------
        Gaussian mixture value for `w_i` to `w_o`.
        """
        res = 0

        for mean, cov, weigth in zip(self.means, self.covs, self.mixing_coefs):
            try:
                res += weigth * multivariate_normal(mean, cov).pdf((wl_i, wl_o))
            except np.linalg.LinAlgError:
                pass

        return res


    def eval_attenuation(self, wl_i: float, wl_o: float) -> float:
        """
        Gets the reconstructed attenuation for specific radiating and 
        reemitting wavelengths.
        
        Parameters
        ----------
        wl_i : float
            Radiating wavelength in nanometers.
        wl_o : float
            Reemitting wavelenght in nanometers.

        Returns
        -------
        Attenuation value for `w_i` to `w_o`.
        """
        return self.scale_attenuation * self.eval_gmm(wl_i, wl_o)


    def eval_MSE(self, origin, reconst) -> float:
        """Calculate sum of mse of each matrix entry."""
        mse = metrics.mean_squared_error(origin, reconst)
        return mse


    def eval_WMSE(self, origin, reconst, weights) -> float:
        """Calculate weighted sum of mse of each matrix entry."""
        wmse = metrics.mean_squared_error(origin, reconst,sample_weight=weights)
        return wmse


    def fit_weight(self, fluo_spectrum: FluoSpectrum, optim_method: str) -> None:
        """
        Performs the weight (scaling) fitting from the current GMM.

        Parameters
        ----------
        fluo_spectrum: FluoSpectrum
            The reference spectrum to use.
        optim_method: string
            Optimization method to use for fitting the scaling, can be 'MSE',
            'MSE_weighted', 'L1' or 'integral'.
        """
        scale_attenuation = 1

        # Filter datasets
        lambda_i = fluo_spectrum.get_excitation_wavelengths()
        lambda_o = fluo_spectrum.get_reflectance_wavelengths()

        original_dataset = np.zeros((len(lambda_o), len(lambda_i)))
        gmm_dataset      = np.zeros((len(lambda_o), len(lambda_i)))
        orig_fluo = fluo_spectrum.get_pure_fluo()

        # Filter datasets
        for i, i_idx in zip(lambda_i, range(len(lambda_i))):
            for o, o_idx in zip(lambda_o, range(len(lambda_o))):
                if o > i:
                    original_dataset[o_idx, i_idx] = max(0, orig_fluo[o_idx, i_idx])
                    gmm_dataset[o_idx, i_idx] = self.eval_gmm(i, o)

        # Find a suitable scaling factor for the reradiation
        if optim_method == 'MSE':
            sol = least_squares(
                optim, 
                [np.sum(original_dataset)/np.sum(gmm_dataset)], 
                kwargs={
                    'original':original_dataset, 
                    'fit':gmm_dataset
                    }
                )
            scale_attenuation = sol.x[0]

        elif optim_method == 'MSE_weighted':
            sol = least_squares(
                optim_weighted, 
                [np.sum(original_dataset)/np.sum(gmm_dataset)], 
                kwargs={
                    'original':original_dataset, 
                    'fit':gmm_dataset,
                    'weights':gmm_dataset
                    }
                )
            scale_attenuation = sol.x[0]

        elif optim_method == 'L1':
            sol = least_squares(
                optim_L1, 
                [np.sum(original_dataset)/np.sum(gmm_dataset)], 
                kwargs={
                    'original':original_dataset, 
                    'fit':gmm_dataset,
                    'weights':gmm_dataset
                    }
                )
            scale_attenuation = sol.x[0]

        elif optim_method == 'integral':
            scale_attenuation = np.sum(original_dataset) / np.sum(gmm_dataset)


        return scale_attenuation

class GMM_sklearn(GMM):
    def __init__(self, 
        n_gaussians: int, 
        optim_method: str, 
        em_method: str,
        fluo_spectrum: FluoSpectrum):
        """
        Creates a GMM from a given fluorescent spectrum using Expectation 
        Minimization.

        The entries of the fluorescent spectrum are duplicated depending on 
        the attenuation value.

        Parameters
        ----------
        n_gaussains: integer
            Number of Gaussians to use in the mixture.
        optim_method: string
            Optimization method to use for fitting the scaling, can be 'MSE',
            'MSE_weighted', 'L1' or 'integral'.
        fluo_spectrum: FluoSpectrum
            The reference spectrum to use.
        """
        from sklearn.mixture import GaussianMixture, BayesianGaussianMixture
        from pipeline.preprocess import make_training_set_m1 
        
        # Get info on the non fluorescent part
        self.start_wl = max(fluo_spectrum.wavelength_i_start, fluo_spectrum.wavelength_o_start)
        self.step_wl = fluo_spectrum.wavelength_i_sampling
        self.diagonal = fluo_spectrum.get_non_fluo()
        
        if np.sum(fluo_spectrum.get_pure_fluo_filtered()) < 1e-4:
            print('No Fluorescence !')
            self.means = []
            self.covs = []
            self.mixing_coefs = []
            self.scale_attenuation = 0
        else:
            if(em_method=='likelihood'):
                gmm = GaussianMixture(
                    n_components=n_gaussians, 
                    covariance_type='full', 
                    init_params='kmeans')
            elif(em_method=='map'):
                gmm = BayesianGaussianMixture(
                    n_components=n_gaussians, 
                    covariance_type='full', 
                    init_params = 'kmeans')

            training_set = make_training_set_m1(fluo_spectrum)

            gmm.fit(training_set)

            self.bic = gmm.bic(training_set)

            self.means = gmm.means_
            self.covs = gmm.covariances_
            self.mixing_coefs = gmm.weights_

            # Get scaling factor for attenuation
            org = fluo_spectrum.get_pure_fluo()

            for i in range(org.shape[0]):
                for j in range(org.shape[1]):
                    if org[i, j] < 0:
                        org[i, j] = 0

            # Find a suitable scaling factor for the reradiation
            self.scale_attenuation = self.fit_weight(fluo_spectrum, optim_method)
            # self.mse = self.eval_MSE(org, self.get_pure_fluo())


class GMM_pomgranate(GMM):
    def __init__(self, 
        n_gaussians: int, 
        optim_method: str, 
        fluo_spectrum: FluoSpectrum):
        """
        Creates a GMM from a given fluorescent spectrum using Weighted 
        Expectation Minimization.

        The entries of the fluorescent spectrum are used as weights.

        Parameters
        ----------
        n_gaussains: integer
            Number of Gaussians to use in the mixture.
        optim_method: string
            Optimization method to use for fitting the scaling, can be 'MSE',
            'MSE_weighted', 'L1' or 'integral'.
        fluo_spectrum: FluoSpectrum
            The reference spectrum to use.
        """
        from pomegranate import GeneralMixtureModel, MultivariateGaussianDistribution

        # Get info on the non fluorescent part
        self.start_wl = max(fluo_spectrum.wavelength_i_start, fluo_spectrum.wavelength_o_start)
        self.step_wl = fluo_spectrum.wavelength_i_sampling
        self.diagonal = fluo_spectrum.get_non_fluo()
        
        if np.sum(fluo_spectrum.get_pure_fluo_filtered()) < 1e-4:
            print('No Fluorescence !')
            self.means = []
            self.covs = []
            self.mixing_coefs = []
            self.scale_attenuation = 0
        else:
            lambda_i = fluo_spectrum.get_excitation_wavelengths()
            lambda_o = fluo_spectrum.get_reflectance_wavelengths()
            ll_i, ll_o = np.meshgrid(lambda_i, lambda_o)

            rerad = np.ravel(fluo_spectrum.get_pure_fluo_filtered())

            # compact the data to make it a training set.
            training_set = np.vstack([np.ravel(ll_i), np.ravel(ll_o), rerad]).T

            # get weights from cleaned data set.
            weights = rerad / np.sum(rerad)

            # compact data
            training_set_cleaned = np.vstack([training_set[:,0], training_set[:,1]]).T

            if n_gaussians == 1: 
                raise Exception("pomegrenate only supports >1 components, switch to sklean!")
            else:
                model = GeneralMixtureModel.from_samples(
                    MultivariateGaussianDistribution,
                    n_components=n_gaussians,
                    X=training_set_cleaned,
                    weights = rerad,
                    #n_init=n_gaussians,
                    #stop_threshold=0.0001
                    )

            model.fit(training_set_cleaned, weights=weights)

            means = []
            covs = []

            for d in model.distributions:
                means.append(d.mu)
                covs.append(d.cov)

            self.means = np.array(means)
            self.covs = np.array(covs)
            self.mixing_coefs = np.exp(model.weights) # pomegranate use log weights

            # Get scaling factor for attenuation
            org = fluo_spectrum.get_pure_fluo()

            for i in range(org.shape[0]):
                for j in range(org.shape[1]):
                    if org[i, j] < 0:
                        org[i, j] = 0

            # Find a suitable scaling factor for the reradiation
            self.scale_attenuation = self.fit_weight(fluo_spectrum, optim_method)

            # Get reconstructed reradiation
            # reconst = self.get_pure_fluo()

            # self.mse = self.eval_MSE(org, reconst)
            # self.wmse = self.eval_WMSE(org, reconst, org)


class GMM_weighted_em(GMM):
    def __init__(self,
        n_gaussians: int,
        optim_method: str,
        fluo_spectrum: FluoSpectrum):

        from gmm._weighted_gaussian_mixture import WeightedGaussianMixture

        # Get info on the non fluorescent part
        self.start_wl = max(fluo_spectrum.wavelength_i_start, fluo_spectrum.wavelength_o_start)
        self.step_wl = fluo_spectrum.wavelength_i_sampling
        self.diagonal = fluo_spectrum.get_non_fluo()
        
        if np.sum(fluo_spectrum.get_pure_fluo_filtered()) < 1e-4:
            print('No Fluorescence !')
            self.means = []
            self.covs = []
            self.mixing_coefs = []
            self.scale_attenuation = 0
        else:
            # Get all the data points in reradiation matrix
            x = np.linspace(
                fluo_spectrum.wavelength_i_start, 
                fluo_spectrum.wavelength_i_end, 
                num=fluo_spectrum.wavelength_i_n_samples)

            y = np.linspace(
                fluo_spectrum.wavelength_o_start, 
                fluo_spectrum.wavelength_o_end, 
                num=fluo_spectrum.wavelength_o_n_samples)

            X, Y = np.meshgrid(x, y)

            # Python specific dimension processing
            X = np.ravel(X)
            Y = np.ravel(Y)
            Z = np.ravel(fluo_spectrum.get_pure_fluo())
            training_set = np.vstack([X,Y,Z]).T
            training_set = training_set[training_set[:,2] > 0.0000]
            # Arrange data for input
            samples = training_set[:, :-1]
            sample_weights = training_set[:,2]

            model_em = WeightedGaussianMixture(
                n_components=n_gaussians, 
                max_iter=1000, 
                random_state=42)

            model_em.fit(samples,sample_weight=sample_weights)

            # Save fitting parameters
            self.means = model_em.means_
            self.covs = model_em.covariances_
            self.mixing_coefs = model_em.weights_

            # Get scaling factor for attenuation
            org = fluo_spectrum.get_pure_fluo()

            for i in range(org.shape[0]):
                for j in range(org.shape[1]):
                    if org[i, j] < 0:
                        org[i, j] = 0

            # Find a suitable scaling factor for the reradiation
            self.scale_attenuation = self.fit_weight(fluo_spectrum, optim_method)

class GMM_weighted_bayesian(GMM):
    def __init__(self,
        n_gaussians: int,
        optim_method: str,
        fluo_spectrum: FluoSpectrum):

        from gmm._weighted_bayesian_mixture import WeightedBayesianGaussianMixture
        from collections import Counter

        # Get info on the non fluorescent part
        self.start_wl = max(fluo_spectrum.wavelength_i_start, fluo_spectrum.wavelength_o_start)
        self.step_wl = fluo_spectrum.wavelength_i_sampling
        self.diagonal = fluo_spectrum.get_non_fluo()
        self.means = []
        self.covs = []
        self.mixing_coefs = []
        
        if np.sum(fluo_spectrum.get_pure_fluo_filtered()) < 1e-4:
            print('No Fluorescence !')
            self.scale_attenuation = 0
        else:
            # Get all the data points in reradiation matrix
            x = np.linspace(
                fluo_spectrum.wavelength_i_start, 
                fluo_spectrum.wavelength_i_end, 
                num=fluo_spectrum.wavelength_i_n_samples)

            y = np.linspace(
                fluo_spectrum.wavelength_o_start, 
                fluo_spectrum.wavelength_o_end, 
                num=fluo_spectrum.wavelength_o_n_samples)

            X, Y = np.meshgrid(x, y)

            # Python specific dimension processing
            X = np.ravel(X)
            Y = np.ravel(Y)
            Z = np.ravel(fluo_spectrum.get_pure_fluo())
            training_set = np.vstack([X,Y,Z]).T
            training_set = training_set[training_set[:,2] > 0.0000]

            # Arrange data for input
            samples = training_set[:, :-1]
            sample_weights = training_set[:,2]

            model = WeightedBayesianGaussianMixture(
                weight_concentration_prior_type="dirichlet_process",
                n_components=n_gaussians, 
                max_iter=1000, 
                random_state=42)

            model.fit(samples,sample_weight=sample_weights)

            # Save fitting parameters
            # Skitlearn didn't provide automatic cut of deplicated fitted cluster for Bayesian fitting, we need to do it by hand.
            # Use the predict label to get each sample is assigned to which cluster, then we can get the correct indice of cluster.
            counter = Counter(model.predict(samples,sample_weight=sample_weights))
            clusters = counter.keys()

            for c in clusters:
                self.means.append(model.means_[c])
                self.covs.append(model.covariances_[c])
                self.mixing_coefs.append(model.weights_[c])

            # Get scaling factor for attenuation
            org = fluo_spectrum.get_pure_fluo()
            
            for i in range(org.shape[0]):
                for j in range(org.shape[1]):
                    if org[i, j] < 0:
                        org[i, j] = 0

            # Find a suitable scaling factor for the reradiation
            self.scale_attenuation = self.fit_weight(fluo_spectrum, optim_method)