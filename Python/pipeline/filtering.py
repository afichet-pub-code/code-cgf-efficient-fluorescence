from radiometry.fluospectrum import FluoSpectrum, RSFluoSpectrum

import numpy as np

def basic_fourier_filter(in_fluo_spectrum):
    pure_fluo = in_fluo_spectrum.get_pure_fluo()
    pure_fluo[pure_fluo < 0] = 0

    # Transform to Fourier domain    
    fourier = np.fft.fft2(pure_fluo)

    # Filter the higher rank harmonics (quite harsh)
    sqr_radius = 60

    for x in range(0, fourier.shape[0]):
        for y in range(0, fourier.shape[1]):
            if (x*x + y*y) > sqr_radius and ((fourier.shape[0] - x)**2 + (y)**2) > sqr_radius:
                fourier[x, y] = 0

    # Get back to temporal domain
    rerad_filtered = np.real(np.fft.ifft2(fourier))

    # Nuke the lambda_o < lambda_i values (unoptimized but works)
    for idx_o in range(rerad_filtered.shape[0]):
        for idx_i in range(rerad_filtered.shape[1]):
            # Zeroing out invalid (nonzero) values under the diagonal
            if in_fluo_spectrum.wl_for_idx_in(idx_i) >= in_fluo_spectrum.wl_for_idx_out(idx_o):
                rerad_filtered.data[idx_o, idx_i] = 0
    
    
    # Copy back the diagonal
    start_wl = max(in_fluo_spectrum.wavelength_i_start, in_fluo_spectrum.wavelength_o_start)
    start_wl_i_idx = in_fluo_spectrum.idx_for_wl_in(start_wl)
    start_wl_o_idx = in_fluo_spectrum.idx_for_wl_out(start_wl)

    for idx_i, idx_o in zip(
                        range(start_wl_i_idx, in_fluo_spectrum.wavelength_i_n_samples), 
                        range(start_wl_o_idx, in_fluo_spectrum.wavelength_o_n_samples)):
        rerad_filtered[idx_o, idx_i] = in_fluo_spectrum.data[idx_o, idx_i]

    # Create the spectrum
    ret_spectrum = RSFluoSpectrum(
        in_fluo_spectrum.wavelength_i_start,
        in_fluo_spectrum.wavelength_i_sampling,
        in_fluo_spectrum.wavelength_o_start,
        in_fluo_spectrum.wavelength_o_sampling,
        rerad_filtered
    )

    return ret_spectrum


def threshold_filter(in_fluo_spectrum, threshold=0.006):
    rerad_filtered = in_fluo_spectrum.get_pure_fluo()
    
    rerad_filtered[rerad_filtered <= threshold] = 0
    
    # Copy back the diagonal
    start_wl = max(in_fluo_spectrum.wavelength_i_start, in_fluo_spectrum.wavelength_o_start)
    start_wl_i_idx = in_fluo_spectrum.idx_for_wl_in(start_wl)
    start_wl_o_idx = in_fluo_spectrum.idx_for_wl_out(start_wl)

    for idx_i, idx_o in zip(
                        range(start_wl_i_idx, in_fluo_spectrum.wavelength_i_n_samples), 
                        range(start_wl_o_idx, in_fluo_spectrum.wavelength_o_n_samples)):
        rerad_filtered[idx_o, idx_i] = in_fluo_spectrum.data[idx_o, idx_i]

    # Create the spectrum
    ret_spectrum = RSFluoSpectrum(
        in_fluo_spectrum.wavelength_i_start,
        in_fluo_spectrum.wavelength_i_sampling,
        in_fluo_spectrum.wavelength_o_start,
        in_fluo_spectrum.wavelength_o_sampling,
        rerad_filtered
    )
    
    
    return ret_spectrum