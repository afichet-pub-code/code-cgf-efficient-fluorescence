# Efficient Storage and Importance Sampling for Fluorescent Reflectance

This repository contains the code used in the paper *Efficient Storage and Importance Sampling for Fluorescent Reflectance* by Qingqin Hua, Vojtěch Tázlar, Alban Fichet, and Alexander Wilkie.

It is provided as is, without any warranty (see LICENSE.txt). Other parts of this repository (data) are licensed differently. When used, please use the respective license.

The pipeline on how to compact the fluorescence data into GMM
representation and perform the sampling in the spectral renderer.

| Folder         | Description                                                   |
|:---------------|:--------------------------------------------------------------|
| `data`         | contains the spectral dataset and illuminates.                 |
| `fits`         | contains the GMM fits of the fluorophores in the spectral dataset.|
| `Python`       | contains the implementation and scripts for paper figures.    |
| `supplemental` | contains the latex template to generate supplemental.         |
| `scenes`       | contains sample scenes for rendering.|
| `renderer`     | contains a patch file to obtain the same source tree as the one used from the paper from a base ART sourcetree|

To run the pipeline, run `render.py`, which will give you the
visualization of the fitting and the rendering result.

