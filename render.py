# This script runs rendering and fitting over the set of fluorescent materials.
# The fitting methods are from CGF version of the paper.
import sys, os, subprocess

sys.path.append(os.path.curdir)
sys.path.append(os.path.join('Python'))
from Python.tools.art import ART
from Python.radiometry import utils
from Python.pipeline.gmm import GMM_weighted_bayesian,GMM_weighted_em
import config 

if __name__ == '__main__':
    if not os.path.exists(config.output_dir):
        os.mkdir(config.output_dir)
    if not os.path.exists(config.reference_dir):
        os.mkdir(config.reference_dir)

    # Initialize renderer
    art =  ART()

    # Load full RIT dataset
    bfc_files = utils.find_bfc_files(config.asset_dir)[1:]
    rit_spectras = utils.load_spectra(bfc_files)

    # Remove the first dataset which is flawed
    rit_spectras = rit_spectras[1:len(rit_spectras)]
    bfc_files = bfc_files[1:len(rit_spectras)]
    
    for scene in config.scenes:
        scene_name = os.path.splitext(os.path.basename(scene))[0]
        for s,f in zip(rit_spectras,bfc_files):
            print('Running for:', os.path.basename(f))

            path_ref_spectrum = os.path.join(config.asset_dir, '{}'.format(os.path.basename(f)))
            path_ref_rendering  = os.path.join(config.reference_dir,'rendering-ref-{}-{}.{}'.format(os.path.basename(f), scene_name, config.ext))

            # Reference rendering
            if config.run_ref_rendering:
                spectrum_ref = 'BFC_COLOUR_MEASUREMENT("{}")'.format(f)
                art.artist(scene, config.n_ref_samples, spectrum_ref, path_ref_rendering)

            # Now, perform the comparison with various number of Gaussians
            for n in config.n_gaussians:
                print('  Gaussians:', str(n))

                # Various paths for the fitted dataset
                path_fit_spectum    = os.path.join(config.output_dir, '{}-{}.gmm'.format(os.path.basename(f), n))
                path_fit_rendering  = os.path.join(config.output_dir, 'rendering-fit-{}-{}-{}.{}'.format(os.path.basename(f), n, scene_name, config.ext))

                if not os.path.exists(path_fit_spectum):
                    print("Running fitting algorithm:",config.alg)
                    fitting = globals()[config.alg](n, config.optim, s)
                    fitting.save(path_fit_spectum)

                # Fitting rendering
                if config.run_fit_rendering:
                    spectrum_tab_cmp = 'GMM_FIT("{}")'.format(path_fit_spectum)
                    art.artist(scene, config.n_samples, spectrum_tab_cmp, path_fit_rendering)