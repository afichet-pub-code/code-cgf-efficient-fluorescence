# Global setting for the pipeline
import os

# Output folder for fitted gmm parameters
output_dir = 'output'
# Output folder for rendering reference
reference_dir = os.path.join('reference')
# Fluorescence data and scene path
asset_dir = os.path.join('data', 'RIT')

# Specify the ART binary
# To get ART binary with compacted GMM, see instructions under "renderer" 
art_dir = ''

# Rendering parameters
n_samples = 64
n_ref_samples = 256
seed = 512
ext = 'exr' #artraw
run_ref_rendering = True
run_fit_rendering = True

# Scene Assets
# SimpleScene: the scene with day light
# SceneUV: the scene with near UV light 
scenes = [
        (os.path.join('scenes', 'SimpleScene.arm')),
        (os.path.join('scenes', 'SceneUV.arm'))
        ]
        
# Fitting parameters
n_gaussians = [2, 4, 6, 8]
# Fitting algorithm
alg = 'GMM_weighted_em' #'GMM_weighted_bayesian'
# Weight for the minimisation process
optim = 'integral'